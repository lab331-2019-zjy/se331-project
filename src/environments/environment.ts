// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  authenticationApi:"http://localhost:8080/auth",
  uploadApi:"http://34.204.49.210:9999/uploadFile"
};


// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
