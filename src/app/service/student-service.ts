import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core'


@Injectable({
    providedIn: 'root'
})

export class StudentService {
    constructor(private firestore: AngularFirestore) { }

    enroll(activity, studentId) {
        let record = {}
        record['activity'] = activity;
        record['studentId'] = studentId;
        return this.firestore.collection("confirmation").add(record);
    }

    register(record) {
        return this.firestore.collection("waiting").add(record);
    }

    update(record, id) {
        return this.firestore.doc('students/' + id).update(record);
    }



}
