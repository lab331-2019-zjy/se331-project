import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { Student } from '../entity/student';
import { StudentService } from '../service/student-service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  username: string;
  currentUser: any;

  public show: boolean = false;
  public buttonName: any = 'Add new comment';

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.host = "Aj.Ball";
    this.location = "CAMT";
    this.comment = "Test";
    this.description = "Test comment";
    this.period = "3 days";
    this.student = "592115508, 592115521"
  }

  toggle() {
    this.show = !this.show;

    // CHANGE THE NAME OF THE BUTTON.
    if (this.show)
      this.buttonName = "Cancel";
    else
      this.buttonName = "Add new comment";
  }

  back() {
    return this._location.back();
  }

  location:any;
  host:any;
  student: any;
  constructor(private router: Router, private studentService: StudentService, private _location: Location)  { }
  comment: any;
  description: any;
  period: any;


}