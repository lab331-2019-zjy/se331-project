import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';


import { BehaviorSubject } from 'rxjs';
import { RegistrationTableDataSource } from './registration-table-datasource';
import { AppService } from '../service/app-service';
import { Student } from '../entity/student';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Student>;
  dataSource: RegistrationTableDataSource;


  displayedColumns = ['id','email','name','surname','image','activities'];
  students: Student[];
  filter: string;
  filter$: BehaviorSubject<string>;
  disableread: boolean[] = [];

  constructor(private appService: AppService) {

  }
  ngOnInit() {


    this.appService.getStudents().subscribe(data => {
      this.students = data.map(e => {
        return {
          name: e.payload.doc.data()['name'],
          surname: e.payload.doc.data()['surname'],
          birthday: e.payload.doc.data()['birthday'],
          email: e.payload.doc.data()['email'],
          image: e.payload.doc.data()['image'],
          password: e.payload.doc.data()['password'],
          activities: e.payload.doc.data()['activities'],
          studentId: e.payload.doc.data()['studentId'],
          id: e.payload.doc.id
        };
      })

      this.dataSource = new RegistrationTableDataSource();
      this.dataSource.data = this.students;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = this.dataSource;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;

    });

  }

  ngAfterViewInit() {
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

}
