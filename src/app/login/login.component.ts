import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AppService } from '../service/app-service';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../service/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  returnUrl: string;
  isError = false;

  addressForm = this.fb.group({
    username: [null, Validators.required],
    password: [null, Validators.required]
  });
  lecturerId: number;

  validation_messages = {
    'username': [
      { type: 'required', message: 'username is <strong>required</strong>' },
    ],
    'password': [
      { type: 'required', message: 'the password is <strong>required</strong>' }
    ]
  };
  hide = true;
  home: boolean;


  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private afAuth: AngularFireAuth,
    private appService: AppService, 
    private auth: AuthService ) {
  }


  ngOnInit(): void {
    this.auth.logout();
    this.isError = false;
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    if (this.returnUrl === '/') {
      this.home = true;
    }
    else {
      this.home = false;
    }
  }


  login() {
    const value = this.addressForm.value;
    this.auth.login(value.username, value.password)
      .subscribe(
        data => {
          this.isError = false;

          if (this.auth.hasRole('ADMIN')) {
            this.router.navigate(["/admin"]);

          }
          else if (this.auth.hasRole('STUDENT')) {
            this.router.navigate(["/student"]);

          }
          else if (this.auth.hasRole('TEACHER')) {
            this.router.navigate(["/teacher"]);

          }
        },
        error => {
          this.isError = true;
        });
    console.log(this.addressForm.value);
  }

  register() {
    return this.router.navigate(['/register']);
  }

}

